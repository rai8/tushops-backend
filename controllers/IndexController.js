const uuid = require('uuid')
const farmers = require('../db/farmers')

//save info of farmers
const saveFarmersInfo = async (req, res) => {
  try {
    //get form contents
    const {
      formData: {
        first_name,
        middle_name,
        last_name,
        email,
        phone_number,
        type_of_farmer,
        farm_size,
      },
    } = req.body

    // console.log('val type', typeof type_of_farmer)
    // console.log(req.body.formData.type_of_farmer)

    //error handler
    if (type_of_farmer.trim.length === 0 && farm_size.trim().length === 0) {
      return res.status(302).json('Kindly fill in Farm size and type of farmer')
    }
    //get sku_produced
    const get_skuProduced = fz => {
      switch (fz) {
        case '500m by 500m - 1000m by 1000m':
          return '1-2'
        case '1000m by 1000m - 3000m by 3000m':
          return '1-3'
        case '3000m by 3000m - 6000m by 6000m':
          return '1-4'
        case 'Above 6000m by 6000m':
          return '1-5 and above'
        default:
          return null
      }
    }

    //get tier
    const get_tier = (type_of_farmer, fz) => {
      if (type_of_farmer === 'Normal Farmer') {
        switch (fz) {
          case '500m by 500m - 1000m by 1000m':
            return 'Bronze'
          case '1000m by 1000m - 3000m by 3000m':
            return 'Silver'
          case '3000m by 3000m - 6000m by 6000m':
            return 'Gold'
          case 'Above 6000m by 6000m':
            return 'Platinum'
          default:
            return null
        }
      } else {
        switch (fz) {
          case '3200 sqft - 5200 sqft':
            return 'Bronze'
          case '1600 sqft - 10720 sqft':
            return 'Silver'
          case '10720 sqft - 21400 sqft':
            return 'Gold'
          case 'Above 21400 sqft':
            return 'Platinum'
          default:
            return null
        }
      }
    }

    //get chicken count
    const get_chickenCount = fz => {
      switch (fz) {
        case '3200 sqft - 5200 sqft':
          return '1600 - 2600'
        case '1600 sqft - 10720 sqft':
          return '2600 - 5360'
        case '10720 sqft - 21400 sqft':
          return '5360 - 10700'
        case 'Above 21400 sqft':
          return '10700 and above'
        default:
          return null
      }
    }

    //get chicken count
    const get_qtyEgg = fz => {
      switch (fz) {
        case '3200 sqft - 5200 sqft':
          return '300 - 500 trays'
        case '1600 sqft - 10720 sqft':
          return '501 - 1000 trays'
        case '10720 sqft - 21400 sqft':
          return '1001 - 2000 trays'
        case 'Above 21400 sqft':
          return '2000 and above'
        default:
          return null
      }
    }

    if (type_of_farmer == 'Normal Farmer') {
      const normalFarmerInfo = {
        id: uuid.v4(),
        first_name,
        middle_name,
        last_name,
        email,
        phone_number,
        type_of_farmer: 'Normal Farmer',
        farm_size,
        sku_produced: get_skuProduced(farm_size),
        tier: get_tier(type_of_farmer, farm_size),
      }

      console.log('normal_farmers', normalFarmerInfo)
      farmers.normal_farmers.unshift(normalFarmerInfo)
    } else {
      const poultryFarmerInfo = {
        id: uuid.v4(),
        first_name,
        middle_name,
        last_name,
        email,
        phone_number,
        type_of_farmer: 'Poultry Farmer',
        farm_size,
        tier: get_tier(type_of_farmer, farm_size),
        chicken_count: get_chickenCount(farm_size),
        qty_produced_weekly: get_qtyEgg(farm_size),
      }

      console.log('poultry farmeers', poultryFarmerInfo)
      farmers.poultry_farmers.unshift(poultryFarmerInfo)
    }
    return res.json(farmers)
  } catch (error) {
    return res.json({ error: error.message })
  }
}

//get farmers details
const getFarmersDetails = async (req, res) => {
  try {
    // res.header('Access-Control-Allow-Origin', '*')

    res.status(200).json(farmers)
  } catch (error) {
    return res.json({ error: error.message })
  }
}
module.exports = { saveFarmersInfo, getFarmersDetails }
