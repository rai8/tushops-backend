//import dependences
const express = require('express')
const cors = require('cors')
const logger = require('morgan')

const PORT = process.env.PORT || 5005

const app = express()
const CLIENT_URL = 'https://venerable-figolla-138477.netlify.app/api/farmers'

//import routes
const indexRouter = require('./routes/index.routes')

//const corsOptions = {
// origin: CLIENT_URL,
// 'Access-Control-Allow-Origin': CLIENT_URL, // update to match the domain you will make the request from
// 'Access-Control-Allow-Headers': CLIENT_URL,
//   'Origin, X-Requested-With, Content-Type, Accept, Authorization',
// 'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS',
//}
//middlewares
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(logger('dev'))
app.use(
  cors({
    origin: '*',
  })
)

//middleware routes
app.use('/api', indexRouter)

//main route
app.get('/', (req, res) => {
  res.send('<h1>Welcome to Tushops backend</h1>')
})
////listen to server
app.listen(PORT, () => console.log('-------server is up and running----------'))
