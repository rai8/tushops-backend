const express = require('express')
const {
  getFarmersDetails,
  saveFarmersInfo,
} = require('../controllers/IndexController')
const router = express.Router()

router.get('/farmers', getFarmersDetails)
router.post('/farmers', saveFarmersInfo)

module.exports = router
